#!/bin/bash

files=$(cd docs && find . -name 'README.md')
cwd=$(pwd)

mkdir -p ./out
for line in $files; do
	outfile=`dirname $(echo $line | cut -d'.' -f2)`
	[[  $outfile == '/' ]] && outfile="/README"
	echo $line $outfile.docx;
	mkdir -p ./out/$(dirname $outfile)
	sh -c "cd ./docs/$outfile && pandoc README.md -f markdown -t docx -s -o $cwd/out/$outfile.docx";
done;

