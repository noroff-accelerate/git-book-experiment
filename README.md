# Template Notes

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://noroff-accelerate.gitlab.io/meta/course-content-template/)

> Course Notes for the Noroff Accelerate Course in .Net

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```sh
$ npm install
```

## Usage

```sh
$ npm start
```

## Maintainers

[Nicholas Lennox (@NicholasLennox)](https://gitlab.com/NicholasLennox)

[Dean von Schoultz (@Deanvons)](https://gitlab.com/Deanvons)

[Craig Marais (@muskatel)](https://gitlab.com/muskatel)

[Greg Linklater (@EternalDeiwos)](https://www.noroff.no/en/contact/staff/53-academic/347-greg-linklater)

[Dewald Els (@sumodevelopment)](https://gitlab.com/sumodevelopment)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

---
Copyright 2021, Noroff Accelerate AS
