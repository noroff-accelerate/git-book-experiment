# Course Overview

By the end of this course you will.....

The **Course** is logically grouped into n **modules**

1.
2.
....

Each **module** is made up of **lessons** 

The Modules and respective lessons are shown below.

## Module 1

This module ...
By the time you have completed this module, you .....

The following lessons are covered in this module.

1. 
2.
....

Lessons in this course provide the content required to achieve relevant learning objectives (detailed at the beginning of each lesson). 

---
Copyright 2022, Noroff Accelerate AS


