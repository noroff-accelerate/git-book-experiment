# Lesson 1 - Title

In this lesson you'll learn about ....

**Learning objectives**

You will be able to...

- **describe** ...
- **apply**  ...
- **design** ....
- .....
---

## Topic 1 

### Sub topic

#### Sub sub topic 

# Text formating rules
- Only use inline code highlighting (`word`) for code keywords which reference code snippets. 
- Use **bold** for emphasis and *italics* for internal reference (to topics, lessons or modules)
- Code snippets - no line number referencing, use comments or explanations to refer to code and keep code snippets concise if possible

> **NOTE** Information important to the topic but does not fit with the flow. Auxiliary info related to the topic/similar topic outside of the scope of the content

> **DOCUMENTATION** Links to official documentation

> **RESOURCE** Repo/worked examples/job aids/how-to guides/glossaries/FAQs/API provided for the course

> **WARNING** Common pitfalls

---

This lesson discussed ......

---
Copyright 2022, Noroff Accelerate AS
